import time
from picamera2 import Picamera2, Preview
from libcamera import controls, Transform

picam = Picamera2()

config = picam.create_preview_configuration()
picam.configure(config)

picam.start_preview(Preview.DRM, x=10, y=-100, width=1250, height=1250, transform=Transform())
picam.start()
picam.set_controls({"AfMode": controls.AfModeEnum.Continuous})

time.sleep(30)

picam.capture_file("test2.jpg")
picam.close()
